
# coding: utf-8

# In[ ]:


# !pip install --upgrade pip
# !pip install --upgrade xgboost
# !pip install scikit-survival


# In[ ]:


import os
import sys
import random
import warnings
import numpy as np
import pandas as pd
import scipy.io as sio

import tensorflow as tf

from tqdm import tqdm
from xgboost import XGBRegressor

from sklearn import preprocessing
from sklearn.utils import resample
from sklearn.model_selection import train_test_split

from sksurv.meta import Stacking
from sksurv.metrics import concordance_index_censored
from sksurv.linear_model import CoxnetSurvivalAnalysis
from sksurv.svm import FastKernelSurvivalSVM, FastSurvivalSVM, NaiveSurvivalSVM
from sksurv.ensemble import ComponentwiseGradientBoostingSurvivalAnalysis, GradientBoostingSurvivalAnalysis


# In[ ]:


if len(sys.argv) is not 4:
    print("python CIndex.py [CancerName] [SymbolName] [FeatureSelectionRatio]")
    print("SymbolName: Clinical, Protein, mRNA, Mutation, CNV")
    print("FeatureSelectionRatio: 0.6 ~ 0.8")
    print("ex: python CIndex.py LGG mRNA 0.6")
    sys.exit(0)


# In[ ]:


warnings.simplefilter("ignore")
# os.environ["CUDA_VISIBLE_DEVICES"]="0"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
tf.logging.set_verbosity(tf.logging.WARN)


# In[ ]:


RandomLoops = 100
# CancerName = "LGG"
CancerName = sys.argv[1]

# Clinical, Protein, mRNA, Mutation, CNVGene, CNVArm"
# SymbolName = "mRNA"
SymbolName = sys.argv[2]

# 0.6 ~ 0.8
# F_Ratio = 0.6
F_Ratio = float(sys.argv[3])


# In[ ]:


random.seed(2131117209522)
train_test_split_random_state = random.sample(range(100000000), 10000)
# print("train test split random state:")
# print(train_test_split_random_state)

with open("%s-%s-random-state.txt" % (CancerName, SymbolName), "w") as text_file:
    text_file.write(str(train_test_split_random_state))


# In[ ]:


AvailableList = np.array([
    "AvailableClinical",
    "AvailableProtein",
    "AvailablemRNA",
    "AvailableMutation",
    "AvailableCNV",
])


# In[ ]:


def get_f_sel_list(CancerName, SymbolName, F_Ratio):
    f_sel = []
    f_sel.append('DeathEvent')
    f_sel.append('SurvivalTime')
    df = pd.read_csv('FeatureSelection/%s-%s-Feature-CIndex.csv'%(CancerName, SymbolName))
    symbols = df.iloc[2:,0].to_frame(name = 'Symbols')
    cox_mean = df.iloc[2:,-2].astype(float).to_frame(name = 'cox_mean')
    xgb_mean = df.iloc[2:,-1].astype(float).to_frame(name = 'xgb_mean')
    xgb_mean[xgb_mean["xgb_mean"] == 0.5] = None
    df1 = pd.concat((symbols, cox_mean, xgb_mean), axis=1)
    df1["sum_mean"] = df1.mean(axis=1)
    df1 = df1.sort_values(by=['sum_mean'], ascending=False)
    df1 = df1[df1['sum_mean']>F_Ratio]
    f_sel.extend(df1.Symbols.tolist())
    
    return f_sel


# In[ ]:


def preproc(data):
    # Rename columns with duplicate names
    cols=pd.Series(data.columns)
    for dup in data.columns.get_duplicates(): cols[data.columns.get_loc(dup)]=[dup+'.'+str(d_idx) if d_idx!=0 else dup for d_idx in range(data.columns.get_loc(dup).sum())]
    data.columns=cols

    ##Drop samples with all value missing
    print("=====data.shape before preprocessing:", data.shape)
    data_surv_dropped = data.drop(["DeathEvent", "SurvivalTime"],axis=1)
    mask = data_surv_dropped.isnull().sum(axis=1) !=len(data_surv_dropped.columns)
    input_data = data[mask]
    print("=====data.shape after dropping samples with all value missing:", input_data.shape)

    print("=====features with missing=====")
    print(input_data.isna().sum()[input_data.isna().sum()>0])

    ##Drop samples with SurvivalValue missing or < 0
    print("=====number of SurvivalTime missing:", input_data.SurvivalTime.isna().sum())
    print("=====number of SurvivalTime <0:", (input_data.SurvivalTime<0).sum())

    input_data = input_data[input_data.SurvivalTime.isnull()==False]
    input_data = input_data[input_data.SurvivalTime>=0]
    print("=====data.shape after dropping samples with SurvivalTime missing or < 0:", input_data.shape)
    
    ##Drop features with missing value
    input_data = input_data.dropna(axis=1)
    print("=====data.shape after dropping features with missing value:",input_data.shape)

    ## Drop features with all identical value 
    cols_iden_value = input_data.columns[input_data.std() == 0]
    input_data = input_data.drop(cols_iden_value,axis=1)
    print("=====data.shape after dropping features with all value identical:",input_data.shape)

    
    ##把 Death Event 轉成 Boolean 值，才能進行 Cox 訓練
    input_data["DeathEvent"] = input_data["DeathEvent"].astype(bool)

    ## Standardize
    input_data.iloc[:,2:] = preprocessing.scale(input_data.iloc[:,2:])

    print("=====features with missing=====(should be none)")
    print(input_data.isna().sum()[input_data.isna().sum()>0])
    
    return input_data


# In[ ]:


def MATtoDF(CancerName):
    mat_data = sio.loadmat("Data/Original/%s.Data.mat" % CancerName)

    available_df = pd.DataFrame(columns=np.insert(AvailableList, 0, "Patients"))
    available_df["Patients"] = mat_data["Samples"]
    
    for AbleType in AvailableList:
        toList = []
        for i in range(len(mat_data[AbleType][0])):
            if mat_data["AvailablemRNA"][0][i][0] == "Yes":
                toList.append(True)
            elif mat_data["AvailablemRNA"][0][i][0] == "No":
                toList.append(False)
        available_df[AbleType] = toList

    available_df.set_index('Patients', inplace = True)

    event_indicator_df_T = pd.DataFrame(1 - mat_data["Censored"], columns=mat_data["Samples"])
    event_indicator_df_T.insert(loc=0, column='Symbols', value=["DeathEvent"])
    event_indicator_df_T.insert(loc=0, column='SymbolTypes', value=["EventIndicator"])
    event_time_df_T = pd.DataFrame(mat_data["Survival"], columns=mat_data["Samples"])
    event_time_df_T.insert(loc=0, column='Symbols', value=["SurvivalTime"])
    event_time_df_T.insert(loc=0, column='SymbolTypes', value=["EventTime"])

    features_df_T = pd.DataFrame(mat_data["Features"], columns=mat_data["Samples"])
    features_df_T.insert(loc=0, column='Symbols', value=list(map(lambda s: s.replace(' ' , ''), mat_data["Symbols"])))
    features_df_T.insert(loc=0, column='SymbolTypes', value=list(map(lambda s: s.replace(' ' , ''), mat_data["SymbolTypes"])))
    pd.unique(features_df_T["SymbolTypes"])

    group_features_df_T = features_df_T.groupby(["SymbolTypes"])
    
    AllDF = {}
    
    AllDF["Clinical"] = pd.concat([event_indicator_df_T, event_time_df_T, group_features_df_T.get_group("Clinical")]).set_index('Symbols').iloc[:,1:].T[available_df["AvailableClinical"]].rename_axis("Patients", axis=0)
    AllDF["Mutation"] = pd.concat([event_indicator_df_T, event_time_df_T, group_features_df_T.get_group("Mutation")]).set_index('Symbols').iloc[:,1:].T[available_df["AvailableMutation"]].rename_axis("Patients", axis=0)
    AllDF["CNVGene"] = pd.concat([event_indicator_df_T, event_time_df_T, group_features_df_T.get_group("CNVGene")]).set_index('Symbols').iloc[:,1:].T[available_df["AvailableCNV"]].rename_axis("Patients", axis=0)
    AllDF["CNVArm"] = pd.concat([event_indicator_df_T, event_time_df_T, group_features_df_T.get_group("CNVArm")]).set_index('Symbols').iloc[:,1:].T[available_df["AvailableCNV"]].rename_axis("Patients", axis=0)
    AllDF["Protein"] = pd.concat([event_indicator_df_T, event_time_df_T, group_features_df_T.get_group("Protein")]).set_index('Symbols').iloc[:,1:].T[available_df["AvailableProtein"]].rename_axis("Patients", axis=0)
    AllDF["mRNA"] = pd.concat([event_indicator_df_T, event_time_df_T, group_features_df_T.get_group("mRNA")]).set_index('Symbols').iloc[:,1:].T[available_df["AvailablemRNA"]].rename_axis("Patients", axis=0)
    
    return AllDF


# In[ ]:


def TSVtoDF(CancerName, SymbolName):
    core_df = pd.read_csv('Data/2014/%s_%s_core.txt' % (CancerName, SymbolName), sep='\t')
    core_df.drop(core_df.columns[core_df.columns.str.contains('unnamed',case = False)],axis = 1,inplace=True)
    core_df.rename(columns={'feature': 'Patient'}, inplace=True)
    core_df.set_index('Patient', inplace=True)

    os_df = pd.read_csv('Data/2014/%s_OS_core.txt' % CancerName, sep='\t')
    os_df.drop(os_df.columns[os_df.columns.str.contains('unnamed',case = False)],axis = 1,inplace=True)
    os_df.rename(columns={'feature': 'Patient'}, inplace=True)
    os_df.set_index('Patient', inplace=True)
    os_df["DeathEvent"] = os_df["OS_vital_status"]
    os_df["SurvivalTime"] = os_df["OS_OS"]
    os_df.drop(["OS_OS","OS_vital_status"],axis = 1,inplace=True)
    
    input_data = os_df.join(core_df)

    train_list_df = pd.read_csv('Data/2014/%s_train_sample_list.txt' % CancerName, sep='\t', header=None)
    test_list_df = pd.read_csv('Data/2014/%s_test_sample_list.txt' % CancerName, sep='\t', header=None)
    
    return input_data, train_list_df, test_list_df


# In[ ]:


def get_c_index(model_name, X_train, y_train, X_test, y_test):
    estimator = model_list[model_name]
    estimator.fit(X_train, y_train)
    
    pred_risk_train = estimator.predict(X_train)
    c_index_train = concordance_index_censored(y_train["DeathEvent"], y_train["SurvivalTime"], pred_risk_train)[0]
    
    pred_risk_test = estimator.predict(X_test)
    c_index_test = concordance_index_censored(y_test["DeathEvent"], y_test["SurvivalTime"], pred_risk_test)[0]
    
    res = {
        "ModelName": model_name,
        "C-index Train": c_index_train,
        "C-index Test": c_index_test,
    }
    
    return res, pred_risk_test


# In[ ]:


class TonyNNv1:
    def __init__(self):
        pass
        
    #Loss Function
    def __negative_log_likelihood(self, E):
        # E = np.array(E).astype(float)
        E = E.astype(float)
        def loss(y_true, risks):
            hazard_ratio = tf.exp(risks)
            log_risk = tf.log(tf.cumsum(hazard_ratio))
            uncensored_likelihood = tf.subtract(risks,log_risk)
            censored_likelihood = uncensored_likelihood * E
            num_observed_events = np.sum(E)
            neg_likelihood = -tf.reduce_sum(censored_likelihood) / num_observed_events 
            return neg_likelihood
        return loss
        
    def fit(self, X, y):
        tf.reset_default_graph()
        
        X = np.array(X)
        E = y["DeathEvent"].astype(int)
        E_bool = y["DeathEvent"]
        y = y["SurvivalTime"]
        
        #Sorting for NNL!
        sort_idx = np.argsort(y)[::-1]
        X = X[sort_idx]
        y = y[sort_idx]
        E = E[sort_idx]
        E_bool = E_bool[sort_idx]
        
        learning_rate = 0.01
        epochs = 100
        
        dnn = tf.Graph()
        self.sess_dnn = tf.Session(graph=dnn)

        with dnn.as_default():
            ##defining placeholders##
            with tf.name_scope('input'):
                self.inputs = tf.placeholder(tf.float64, [None, X.shape[1]], name='input_data')
                targets = tf.placeholder(tf.float64, [None], name='targets')

            ##dense layer##
            with tf.variable_scope("dense_layer1"):
                dense_layer1 = tf.layers.dense(inputs= self.inputs, units= 128,activation=tf.nn.relu)
            with tf.variable_scope("dense_layer2"):
                dense_layer2 = tf.layers.dense(dense_layer1, 256,activation=tf.nn.relu)
        #     with tf.variable_scope("dense_layer3"):
        #         dense_layer3 = tf.layers.dense(dense_layer2, 512,activation=tf.nn.relu)
        #     with tf.variable_scope("dense_layer4"):
        #         dense_layer4 = tf.layers.dense(dense_layer3, 512,activation=tf.nn.relu)
        #     with tf.variable_scope("dense_layer5"):
        #         dense_layer5 = tf.layers.dense(dense_layer4, 512,activation=tf.nn.relu)
        #     with tf.variable_scope("dense_layer6"):
        #         dense_layer6 = tf.layers.dense(dense_layer5, 512,activation=tf.nn.relu)
            with tf.variable_scope("dense_layer7"):
                dense_layer7 = tf.layers.dense(dense_layer2, 256,activation=tf.nn.relu)
            with tf.variable_scope("dense_layer8"):
                dense_layer8 = tf.layers.dense(dense_layer7, 256,activation=tf.nn.relu)
            with tf.variable_scope("dense_layer9"):
                dense_layer9 = tf.layers.dense(dense_layer8, 256,activation=tf.nn.relu)

            ##Output layer##   
            with tf.variable_scope('output_layer'):
                self.logits = tf.layers.dense(dense_layer9, 1)
                self.risks = tf.reshape(self.logits, (-1,))
            ##loss and optimization##
            with tf.name_scope('loss_and_opt'):
                loss = self.__negative_log_likelihood(E)(None, self.risks)
                optimizer = tf.train.GradientDescentOptimizer(learning_rate)
                train_optimizer = optimizer.minimize(loss)
                
            init = tf.global_variables_initializer()
            
        self.sess_dnn.run(init)

        for i in range(epochs):
            traind_scores = []

            l, _, r = self.sess_dnn.run([loss, train_optimizer, self.risks], feed_dict={self.inputs:X, targets:y})
            try:
                c_index = concordance_index_censored(E_bool,list(y),list(r))[0]
            except:
                c_index = np.nan
                
            # if (i % 10) == 0:
                # print('Epoch {}/{}'.format(i+1, epochs), 'loss: {}'.format(l),  ' c_index: {}'.format(c_index))
        
    def predict(self, X):
        # risks = tf.reshape(self.logits, (X.shape[0],))
        return self.sess_dnn.run([self.risks], feed_dict={self.inputs:X})[0]
    
    def save(self, path):
        saver = tf.train.Saver()
        return saver.save(self.sess_dnn, path)


# In[ ]:


class TonyNNv2:
    def __init__(self):
        pass
        
    #Loss Function
    def __negative_log_likelihood(self, E):
        # E = np.array(E).astype(float)
        E = E.astype(float)
        def loss(y_true, risks):
            hazard_ratio = tf.exp(risks)
            log_risk = tf.log(tf.cumsum(hazard_ratio))
            uncensored_likelihood = tf.subtract(risks,log_risk)
            censored_likelihood = uncensored_likelihood * E
            num_observed_events = np.sum(E)
            neg_likelihood = -tf.reduce_sum(censored_likelihood) / num_observed_events 
            return neg_likelihood
        return loss
        
    def fit(self, X, y):
        tf.reset_default_graph()
        
        X = np.array(X)
        E = y["DeathEvent"].astype(int)
        E_bool = y["DeathEvent"]
        y = y["SurvivalTime"]
        
        #Sorting for NNL!
        sort_idx = np.argsort(y)[::-1]
        X = X[sort_idx]
        y = y[sort_idx]
        E = E[sort_idx]
        E_bool = E_bool[sort_idx]
        
        learning_rate = 0.001
        dropout_rate = 0.5
        epochs = 200
        
        dnn = tf.Graph()
        self.sess_dnn = tf.Session(graph=dnn)

        with dnn.as_default():
            ##defining placeholders##
            with tf.name_scope('input'):
                self.inputs = tf.placeholder(tf.float64, [None, X.shape[1]], name='input_data')
                targets = tf.placeholder(tf.float64, [None], name='targets')

            ##dense layer##
            with tf.variable_scope("dense_layer1"):
                dense_layer1 = tf.layers.dense(inputs= self.inputs, units= 2048,activation=tf.nn.relu)
                dropout_layer1 = tf.layers.dropout(dense_layer1, dropout_rate)
            with tf.variable_scope("dense_layer2"):
                dense_layer2 = tf.layers.dense(dropout_layer1, 2048,activation=tf.nn.relu)
                dropout_layer2 = tf.layers.dropout(dense_layer2, dropout_rate)
            with tf.variable_scope("dense_layer3"):
                dense_layer3 = tf.layers.dense(dropout_layer2, 2048,activation=tf.nn.relu)
                dropout_layer3 = tf.layers.dropout(dense_layer3, dropout_rate)

            ##Output layer##   
            with tf.variable_scope('output_layer'):
                self.logits = tf.layers.dense(dropout_layer3, 1)
                self.risks = tf.reshape(self.logits, (-1,))
            ##loss and optimization##
            with tf.name_scope('loss_and_opt'):
                loss = self.__negative_log_likelihood(E)(None, self.risks)
                optimizer = tf.train.RMSPropOptimizer(learning_rate)
                # optimizer = tf.train.GradientDescentOptimizer(learning_rate)
                train_optimizer = optimizer.minimize(loss)
                
            init = tf.global_variables_initializer()
            
        self.sess_dnn.run(init)

        for i in range(epochs):
            traind_scores = []

            l, _, r = self.sess_dnn.run([loss, train_optimizer, self.risks], feed_dict={self.inputs:X, targets:y})
            try:
                c_index = concordance_index_censored(E_bool,list(y),list(r))[0]
            except:
                c_index = np.nan
                
            # if (i % 10) == 0:
                # print('Epoch {}/{}'.format(i+1, epochs), 'loss: {}'.format(l),  ' c_index: {}'.format(c_index))
        
    def predict(self, X):
        # risks = tf.reshape(self.logits, (X.shape[0],))
        return self.sess_dnn.run([self.risks], feed_dict={self.inputs:X})[0]
    
    def save(self, path):
        saver = tf.train.Saver()
        return saver.save(self.sess_dnn, path)


# In[ ]:


class TonyConv1d:
    def __init__(self):
        pass
        
    #Loss Function
    def __negative_log_likelihood(self, E):
        # E = np.array(E).astype(float)
        E = E.astype(float)
        def loss(y_true, risks):
            hazard_ratio = tf.exp(risks)
            log_risk = tf.log(tf.cumsum(hazard_ratio))
            uncensored_likelihood = tf.subtract(risks,log_risk)
            censored_likelihood = uncensored_likelihood * E
            num_observed_events = np.sum(E)
            neg_likelihood = -tf.reduce_sum(censored_likelihood) / num_observed_events 
            return neg_likelihood
        return loss
        
    def fit(self, X, y):
        tf.reset_default_graph()
        
        X = np.array(X)
        E = y["DeathEvent"].astype(int)
        E_bool = y["DeathEvent"]
        y = y["SurvivalTime"]
        
        #Sorting for NNL!
        sort_idx = np.argsort(y)[::-1]
        X = X[sort_idx]
        y = y[sort_idx]
        E = E[sort_idx]
        E_bool = E_bool[sort_idx]
        
        learning_rate = 0.001
        kernel_size = 7
        epochs = 200
        
        dnn = tf.Graph()
        self.sess_dnn = tf.Session(graph=dnn)

        with dnn.as_default():
            ##defining placeholders##
            with tf.name_scope('input'):
                self.inputs = tf.placeholder(tf.float32, [None, X.shape[1]], name='input_data')
                targets = tf.placeholder(tf.float32, [None], name='targets')
            
            ##conv layers##
            with tf.variable_scope("conv_layer1"):
                dim_expan = tf.expand_dims(self.inputs,2)
                conv_layer1 = tf.layers.conv1d(dim_expan, 64, kernel_size=kernel_size, strides=1, activation=tf.nn.relu,padding="same")
            with tf.variable_scope("conv_layer2"):
                conv_layer2 = tf.layers.conv1d(conv_layer1, 64, kernel_size=kernel_size, strides=1, activation=tf.nn.relu,padding="same")
                flatten = tf.layers.flatten(conv_layer2)

            ##Output layer##   
            with tf.variable_scope('output_layer'):
                self.logits = tf.layers.dense(flatten, 1)
                self.risks = tf.reshape(self.logits, (-1,))
            ##loss and optimization##
            with tf.name_scope('loss_and_opt'):
                loss = self.__negative_log_likelihood(E)(None, self.risks)
                optimizer = tf.train.RMSPropOptimizer(learning_rate)
                # optimizer = tf.train.GradientDescentOptimizer(learning_rate)
                train_optimizer = optimizer.minimize(loss)
                
            init = tf.global_variables_initializer()
            
        self.sess_dnn.run(init)

        for i in range(epochs):
            traind_scores = []

            l, _, r = self.sess_dnn.run([loss, train_optimizer, self.risks], feed_dict={self.inputs:X, targets:y})
            try:
                c_index = concordance_index_censored(E_bool,list(y),list(r))[0]
            except:
                c_index = np.nan
                
            # if (i % 10) == 0:
                # print('Epoch {}/{}'.format(i+1, epochs), 'loss: {}'.format(l),  ' c_index: {}'.format(c_index))
        
    def predict(self, X):
        # risks = tf.reshape(self.logits, (X.shape[0],))
        return self.sess_dnn.run([self.risks], feed_dict={self.inputs:X})[0]
    
    def save(self, path):
        saver = tf.train.Saver()
        return saver.save(self.sess_dnn, path)


# In[ ]:


class XGBoostSurvival:
    def __init__(self):
        self.xgb = XGBRegressor(
            objective = 'survival:cox',
            eval_metric = 'cox-nloglik',
            max_depth=10,
            n_estimators=2000,
        )
        
    def fit(self, X, y):
        self.xgb.fit(X, y["SurvivalTime"], y["DeathEvent"])
        
    def predict(self, X):
        return self.xgb.predict(X)


# In[ ]:


AllDF = MATtoDF(CancerName)
input_data = AllDF[SymbolName]
print("Input_data Shape:")
print(input_data.shape)


# In[ ]:


input_data = preproc(input_data)
print("Standardized Input_data Shape:")
print(input_data.shape)


# In[ ]:


f_sel_list = get_f_sel_list(CancerName, SymbolName, F_Ratio)
input_data = input_data[f_sel_list]
print("Feature Selection Input_data Shape:")
print(input_data.shape)


# In[ ]:


input_data.head()


# In[ ]:


# FeatureList 為特徵選取後的清單
# y 值為 DeathEvent、SurvivalTime
X = input_data.iloc[:,2:]
y = input_data.iloc[:,:2].to_records(index = False)


# In[ ]:


all_c_index = pd.DataFrame(columns=["ModelName", "C-index Train", "C-index Test"])


# In[ ]:


all_km_data = input_data.iloc[:,:2]
columns=[("BasicData","DeathEvent"),("BasicData","SurvivalTime")]
all_km_data.columns=pd.MultiIndex.from_tuples(columns)


# In[ ]:


now = 0
error = 0
pbar = tqdm(total=RandomLoops)
while (now < RandomLoops):
    model_list = {
        "stacking_a": Stacking(
            meta_estimator=FastKernelSurvivalSVM(kernel="cosine"),
            base_estimators=[
                ["coxnet",CoxnetSurvivalAnalysis()],
                ["cgbs",ComponentwiseGradientBoostingSurvivalAnalysis()],
                ["gbs",GradientBoostingSurvivalAnalysis()],
                ["fksvm_linear",FastKernelSurvivalSVM(kernel="linear")],
                ["fksvm_poly",FastKernelSurvivalSVM(kernel="poly")],
                ["fksvm_rbf",FastKernelSurvivalSVM(kernel="rbf")],
                ["fksvm_cosine",FastKernelSurvivalSVM(kernel="cosine")],
                ["fsvm",FastSurvivalSVM()],
                # ["nsvm",NaiveSurvivalSVM()],
            ]
        ),
        "tony_nn_v1": TonyNNv1(),
        "tony_nn_v2": TonyNNv2(),
        "tony_conv1d": TonyConv1d(),
        "xgb_surv": XGBoostSurvival(),
        "coxnet": CoxnetSurvivalAnalysis(),
        "cgbs": ComponentwiseGradientBoostingSurvivalAnalysis(),
        "gbs": GradientBoostingSurvivalAnalysis(),
        "fksvm_linear": FastKernelSurvivalSVM(kernel="linear"),
        "fksvm_poly": FastKernelSurvivalSVM(kernel="poly"),
        "fksvm_rbf": FastKernelSurvivalSVM(kernel="rbf"),
        "fksvm_cosine": FastKernelSurvivalSVM(kernel="cosine"),
        "fsvm": FastSurvivalSVM(),
        # "nsvm": NaiveSurvivalSVM(),
    }
    
    X_train, X_test, y_train, y_test = train_test_split(
        X, y,
        test_size=0.20,
        random_state=train_test_split_random_state[now+error]
    )
    
    try:
        for model_name in model_list:
            c_index, risk = get_c_index(model_name, X_train, y_train, X_test, y_test)
            all_c_index = all_c_index.append(c_index, ignore_index=True)
            risk_df = X_test.iloc[:,:2]
            columns=[("BasicData","DeathEvent"),("BasicData","SurvivalTime")]
            risk_df.columns=pd.MultiIndex.from_tuples(columns)
            risk_df[model_name, now+error] = risk
            risk_df.drop("BasicData", axis=1, inplace=True)
            all_km_data = all_km_data.join(risk_df)
        now += 1
        pbar.update(1)
    except:
        error += 1
        print("Some Error, Seed No: %s, Seed: %s" % (now+error, train_test_split_random_state[now+error]))
        continue
        
pbar.close()


# In[ ]:


for model_name in model_list:
    all_km_data[model_name,"mean"] = all_km_data[model_name].mean(axis=1)
all_km_data.dropna(subset=[(model_name,"mean")]).to_csv("%s-%s-KM.csv" % (CancerName, SymbolName))


# In[ ]:


all_c_index.to_csv("%s-%s-CIndex.csv" % (CancerName, SymbolName), index=False)


# In[ ]:


all_c_index.groupby("ModelName").describe().to_csv("%s-%s-CIndex-Des.csv" % (CancerName, SymbolName))


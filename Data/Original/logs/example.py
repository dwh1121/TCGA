from tcgaintegrator.BuildDataset import BuildDataset
import os

NowPath = os.path.dirname(os.path.abspath(__file__))
Output = '%s/Data/' % NowPath
FirehosePath = None

DiseaseList = [
    "BRCA",
    "GBM",
    "OV",
    "LUAD",
    "UCEC",
    "KIRC",
    "HNSC",
    "LGG",
    "THCA",
    "LUSC",
    "PRAD",
    "SKCM",
    "COAD",
    "STAD",
    "BLCA",
    "LIHC",
    "CESC",
    "KIRP",
    "SARC",
    "LAML",
    "ESCA",
    "PAAD",
    "PCPG",
    "READ",
    "TGCT",
    "THYM",
    "KICH",
    "ACC",
    "MESO",
    "UVM",
    "DLBC",
    "UCS",
    "CHOL",
]

CancerCensusFile = None
MutsigQ = None
GisticQ = None

for Disease in DiseaseList:
    try:
        BuildDataset(Output, FirehosePath, Disease, CancerCensusFile, MutsigQ, GisticQ)
    except IndexError:
        print("%s IndexError!!!" % Disease)
    except ValueError:
        print("%s ValueError!!!" % Disease)
    except:
        print("%s UnexceptError!!!" % Disease)